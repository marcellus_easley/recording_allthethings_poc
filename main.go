package main

// 1021754e-c2aa-11e9-9611-f2fb482fa531/20190916/1568666709.d61adbd8-d8c2-11e9-a6ea-6a0fed8be683_0.wav
import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"regexp"
	"strconv"

	"bitbucket.org/AVOXI/sdp-common-util/logger"
	"cloud.google.com/go/storage"
	"google.golang.org/api/iterator"
)

const (
	bucketName = "sdp-test-recordings"
)

var (
	bucket        *storage.BucketHandle
	aggregateData map[string]int64
)

func main() {

	ctx := context.Background()

	query := &storage.Query{Prefix: ""}
	query.SetAttrSelection([]string{"Name, Metadata"})

	client, err := storage.NewClient(ctx)
	if err != nil {
		logger.Error("failed to get client", "error", err)
	}

	bucket = client.Bucket(bucketName)

	//getFiles(ctx, query, 100000)
	results := getFileMetaData(ctx, query, 500000)

	PrettyPrint(results)

}

func getOrgId(s string) string {
	regex := regexp.MustCompile(`^([A-Za-z0-9])\w+\-([A-Za-z0-9])\w+\-([A-Za-z0-9])\w+\-([A-Za-z0-9])\w+\-([A-Za-z0-9])\w+`)
	orgIdLength := 36

	matches := regex.FindAllString(s, -1)
	if len(matches) < 1 {
		return ""
	}
	if len(matches[0]) < orgIdLength {
		return ""
	}
	return matches[0]
}

func getFileMetaData(ctx context.Context, query *storage.Query, max int64) map[string]int64 {

	entries := make(map[string]int64)
	var numFiles int64
	duration := int64(0)
	it := bucket.Objects(ctx, query)
	for {
		attrs, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			logger.Error("something bad happened", "error", err)
			os.Exit(1)
		}

		numFiles++

		orgID := getOrgId(attrs.Name)
		if orgID == "" {
			continue
		}

		entity := attrs.Metadata["entity"]
		entity_id := attrs.Metadata["entity_id"]
		if attrs.Metadata["duration"] == "" {
			duration = 0
		} else {
			duration, err = strconv.ParseInt(attrs.Metadata["duration"], 10, 64)
			if err != nil {
				logger.Error("Error converting duration from string to int64", "error", err, "filename", attrs.Name)
				continue
			}
		}

		if entity == "" || entity_id == "" || duration == 0 {
			continue
		}

		fmt.Println("num files:", numFiles, attrs.Name, "metadata:", attrs.Metadata)
		entries[createKey(orgID, entity, entity_id)] += duration
		if len(entries) == 3 {
			break
		}
		if numFiles > max {
			break
		}

	}
	return entries
}

func createKey(orgID string, entity string, entity_id string) string {
	return fmt.Sprintf("%s:%s:%s", orgID, entity, entity_id)
}

func getFiles(ctx context.Context, query *storage.Query, max int64) {
	var names []string
	var numFiles int64
	it := bucket.Objects(ctx, query)
	for {
		attrs, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			logger.Error("something bad happened", "error", err)
			os.Exit(1)
		}
		names = append(names, attrs.Name)
		numFiles++
		logger.Info("File data", "Name: ", attrs.Name, "Content Type", attrs.ContentType, "Metadata", attrs.Metadata)
		if numFiles == max {
			break
		}

	}
	logger.Info("Total Number of files", "number (len(names))", len(names), "numFiles", numFiles)
}

func PrettyPrint(v interface{}) (err error) {
	b, err := json.MarshalIndent(v, "", "  ")
	if err == nil {
		fmt.Println(string(b))
	}
	return
}
